# Hej og velkommen!
Dette er min første repository til faget ÆP..

## header 2
### header 3
###### header 6

**dette er en fed tekst** modsat den normale tekst, som ser sådan her ud <br>
_sådan her ser en skråskrift ud_, er den ikke flot?

> kodning er mega sjovt

Jeg elsker funktionen `ellipse()` fordi den laver cirkler.
```
Dette er en hel kode block
den kan være flere
linjer lang
```

- liste 1
- liste 2
    - liste i liste

[klick på dette link](https://en.wikipedia.org/wiki/Tag_(metadata)#cite_ref-1)

![](https://media.giphy.com/media/pqLYduCqGG5yRXvnNN/giphy-downsized-large.gif)
